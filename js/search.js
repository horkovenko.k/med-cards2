import {getUsers} from "./queriesAPI.js";

const filterOpenButton = document.getElementById('filter-open');
const filterCloseButton = document.getElementById('filter-closed');
const filterLowButton = document.getElementById('filter-low');
const filterDefaultButton = document.getElementById('filter-default');
const filterHighButton = document.getElementById('filter-high');
const searchButton = document.getElementById('search-button');
const resetButton = document.getElementById('reset-button');
const searchInput = document.getElementById('search-input');
const alertForNoItemsInDatabase = document.getElementById('alert-cards');
const footerElement = document.getElementById('footer');

const cardsList = document.getElementsByClassName('col-sm-3');

export function showAlert() {
    if (document.getElementsByClassName('col-sm-3').length === 0) {
        alertForNoItemsInDatabase.classList.remove('dn');
        footerElement.classList.add('footer-bottom');
    } else {
        alertForNoItemsInDatabase.classList.add('dn');
        footerElement.classList.remove('footer-bottom');
    }
}

function searchPatient(status, priority, searchText) {
    [...cardsList].filter(item => {
        if (item.querySelector('.card-status').innerText !== status && status !== '' ) {
            item.remove()
        }

        if (item.querySelector('.card-priority').innerText !== priority && priority !== '') {
            item.remove()
        }

        if (
            searchText !== '' &&
            !item.querySelector('.card-doctor').innerText.toLowerCase().includes(searchText.toLowerCase()) &&
            !item.querySelector('.card-date').innerText.toLowerCase().includes(searchText.toLowerCase()) &&
            !item.querySelector('.card-name').innerText.toLowerCase().includes(searchText.toLowerCase()) &&
            !item.querySelector('.card-age').innerText.toLowerCase().includes(searchText.toLowerCase()) &&
            !item.querySelector('.card-status').innerText.toLowerCase().includes(searchText.toLowerCase()) &&
            !item.querySelector('.card-priority').innerText.toLowerCase().includes(searchText.toLowerCase()) &&
            !item.querySelector('.card-cause').innerText.toLowerCase().includes(searchText.toLowerCase())
        ) {
            item.remove()
        }
    })
}

let statusCurrent = '';
let priorityCurrent = '';
let searchTextCurrent = '';

window.addEventListener('click', function (e) {
    switch (e.target.id) {
        case 'filter-open':
            statusCurrent = "Open";
            getUsers().then(res => {
                searchPatient(statusCurrent, priorityCurrent, searchTextCurrent);
                showAlert();
            });
            break;
        case 'filter-closed':
            statusCurrent = "Closed";
            getUsers().then(res => {
                searchPatient(statusCurrent, priorityCurrent, searchTextCurrent);
                showAlert();
            });
            break;
        case 'search-button':
            searchTextCurrent = searchInput.value;
            getUsers().then(res => {
                searchPatient(statusCurrent, priorityCurrent, searchTextCurrent);
                showAlert();
            });
            break;
        case 'filter-low':
            priorityCurrent = "Low";
            getUsers().then(res => {
                searchPatient(statusCurrent, priorityCurrent, searchTextCurrent);
                showAlert();
            });
            break;
        case 'filter-default':
            priorityCurrent = "Default";
            getUsers().then(res => {
                searchPatient(statusCurrent, priorityCurrent, searchTextCurrent);
                showAlert();
            });
            break;
        case 'filter-high':
            priorityCurrent = "High";
            getUsers().then(res => {
                searchPatient(statusCurrent, priorityCurrent, searchTextCurrent);
                showAlert();
            });
            break;
    }

});

searchButton.onclick = function() {
    searchPatient(statusCurrent, priorityCurrent, searchTextCurrent);
};

resetButton.onclick = function () {
    statusCurrent = '';
    priorityCurrent = '';
    searchTextCurrent = '';

    searchInput.value = '';
    getUsers().then(res => {
        showAlert();
    });
};
