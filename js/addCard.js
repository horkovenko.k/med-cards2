import Visit from "./Visit.js";
import { showAlert } from "./search.js";

export const modalWindow = document.getElementById('create-card');
export const modalForm = document.getElementById('modal');
export const buttonWhichAddsCard = document.getElementById('create-btn');
export const buttonWhichEditsCard = document.getElementById('edit-btn');
const buttonWhichOpenModal = document.getElementById('add-appointment');
const buttonWhichClosesModal = document.getElementById('close-btn');

export const inputValues = document.getElementById('modal').getElementsByClassName('user-data');
let ids = document.querySelectorAll('.col-sm-3').length;

buttonWhichOpenModal.onclick = function () {
    modalWindow.classList.remove('dn');
};

buttonWhichClosesModal.onclick = function () {
    modalWindow.classList.add('dn');
    buttonWhichEditsCard.disabled = true;
    buttonWhichAddsCard.disabled = false;
    modalForm.reset();
};

buttonWhichAddsCard.onclick = function () {
    let isValid = true;

    let [doctor, status, priority, date, name, age, cause] = inputValues;

    [...inputValues].forEach(item => {
        if (item.value.length < 1) {
            isValid = false;
        }
    });

    if(isValid){
        let visit = new Visit(doctor.value, date.value, name.value, age.value, status.value, priority.value, cause.value);
        visit.renderCardAndAddItToDatabase();
        modalForm.reset();
        showAlert();
    }
};

modalWindow.addEventListener('click', function (e) {
    if (e.target.classList.contains('create-card')) {
        this.classList.add('dn');
        buttonWhichEditsCard.disabled = true;
        buttonWhichAddsCard.disabled = false;
        modalForm.reset();
    }
});
