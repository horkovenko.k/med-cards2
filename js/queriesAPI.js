import Visit from "./Visit.js";

const endpoint = "http://localhost:4000/api/users/";

export async function createUser(doctor, date, name, age, status, priority, cause ) {
    const response = await fetch(endpoint, {
        method: "POST",
        headers: { "Accept": "application/json", "Content-Type": "application/json" },
        body: JSON.stringify({
            doctor: doctor,
            date: date,
            name: name,
            age: age,
            status: status,
            priority: priority,
            cause: cause
        })
    });
}

export async function getUsers() {
    const response = await fetch(endpoint, {
        method: "GET",
        headers: { "Accept": "application/json" }
    });

    if (response.ok === true) {
        [...document.getElementsByClassName('col-sm-3')].forEach(item => item.remove());
        const users = await response.json();
        users.forEach(item => {
            let visit = new Visit(item.doctor, item.date, item.name, item.age, item.status, item.priority, item.cause);
            visit.renderAllVisitCards();
        })

    }
}

export async function deleteUser(id) {
    await fetch(endpoint + id, {
        method: "DELETE",
        headers: { "Accept": "application/json" }
    })
}

export async function editUser(id, doctor, date, name, age, status, priority, cause) {
    const response = await fetch(endpoint, {
        method: "PUT",
        headers: { "Accept": "application/json", "Content-Type": "application/json" },
        body: JSON.stringify({
            id: id,
            doctor: doctor,
            date: date,
            name: name,
            age: age,
            status: status,
            priority: priority,
            cause: cause
        })
    });
}