import { getUsers } from "./queriesAPI.js";
import {showAlert} from "./search.js";

const authSection = document.getElementById('authorization');
const apiBody = document.getElementById('api-body');
const email = document.getElementById('email');
const password = document.getElementById('password');
const stayInSwitch = document.getElementById('stay-in');
const signInBtn = document.getElementById('sign-in');
const signOutBtn = document.getElementById('sign-out');
const alertAuth = document.getElementById('alert-auth');

const hideAuthForm = () => {
    authSection.classList.add('dn');
    apiBody.classList.remove('dn');
};

const showAuthForm = () => {
    authSection.classList.remove('dn');
    apiBody.classList.add('dn');
};

const toggleAuthLabel = () => {
    alertAuth.classList.toggle('dn');
};

localStorage.getItem('auth') === 'not' ? showAuthForm() : hideAuthForm();

getUsers().then(res => {
    showAlert();
});

signInBtn.onclick = function () {
    if (email.value === 'name@example.com' && password.value === 'password') {
        hideAuthForm();
        stayInSwitch.checked ? localStorage.setItem('auth', email.value) : localStorage.setItem('auth', 'not');
    } else {
        toggleAuthLabel();
        setTimeout(toggleAuthLabel, 1000);
    }
};

signOutBtn.onclick = function () {
    localStorage.setItem('auth', 'not');

    showAuthForm();
};