import { createUser, deleteUser, editUser, getUsers } from "./queriesAPI.js";
import { buttonWhichAddsCard, buttonWhichEditsCard, modalWindow, inputValues } from "./addCard.js";
import {showAlert} from "./search.js";

export default class Visit {
    constructor(doctor, date, name, age, status, priority, cause) {
        this.doctor = doctor;
        this.date = date;
        this.name = name;
        this.age = age;
        this.status = status;
        this.priority = priority;
        this.cause = cause;

        this.card = document.createElement(`div`);
        this.card.classList.add("card", "bg-light", "mb-3", "col-sm-3");

        this.card.innerHTML = `
            <div>
            <p class="card-header card-doctor">${doctor}</p>
            
            <div class="btn-group ed" role="group" style="width: 100%; padding: 5px" aria-label="Basic example">
              <button type="button" class="btn btn-warning edit-button">Edit</button>
              <button type="button" class="btn btn-danger delete-button">Delete</button>
            </div>
            </div>
            <div class="card-body" style="overflow: auto;">
                <h5 class="card-title"><span class="bt">Visit date:&nbsp;</span><span class="card-date">${date}</span></h5>
                <p class="card-text" ><span class="bt">Patient name:&nbsp;</span><span class="card-name">${name}</span></p>
                <p class="card-text"><span class="bt">Patient age:&nbsp;</span><span class="card-age">${age}</span></p>
                <p class="card-text"><span class="bt">Visit status:&nbsp;</span><span class="card-status">${status}</span></p>
                <p class="card-text"><span class="bt">Visit&nbsp;priority:&nbsp;</span><span class="card-priority">${priority}</span></p>
                <p class="card-text"><span class="bt">Visit&nbsp;reason:&nbsp;</span><span class="card-cause">${cause}</span></p>
            </div>`;
    }

    renderCardAndAddItToDatabase() {
        const {doctor, date, name, age, status, priority, cause} = this;
        createUser(doctor, date, name, age, status, priority, cause);
        this.listener();
        document.querySelector('#app-cards').appendChild(this.card);
    }

    renderAllVisitCards() {
        const {doctor, date, name, age, status, priority, cause} = this;

        this.listener();
        document.querySelector('#app-cards').appendChild(this.card);
    }

     listener() {
        this.card.addEventListener('click', function (e) {

            if (e.target.classList.contains('delete-button')) {

                let itemToDelete = [...document.getElementsByClassName('delete-button')].indexOf(e.target);

                (async function invokeDeleteCard() {
                    const response = await fetch('http://localhost:4000/api/users/');

                    const users = await response.json();

                    await deleteUser(users[itemToDelete]._id);

                    getUsers().then(res => {
                        showAlert();
                    });

                })()

            }

            if (e.target.classList.contains('edit-button')) {
                let itemToEdit = [...document.getElementsByClassName('edit-button')].indexOf(e.target);

                let modalDoctor = document.getElementById('doctor');
                let modalDate = document.getElementById('date');
                let modalName = document.getElementById('patient-name');
                let modalAge = document.getElementById('patient-age');
                let modalStatus = document.getElementById('status');
                let modalPriority = document.getElementById('priority');
                let modalReason = document.getElementById('patient-reason');

                let cardDoctor = document.getElementsByClassName('card-doctor')[itemToEdit];
                let cardDate = document.getElementsByClassName('card-date')[itemToEdit];
                let cardName = document.getElementsByClassName('card-name')[itemToEdit];
                let cardAge = document.getElementsByClassName('card-age')[itemToEdit];
                let cardStatus = document.getElementsByClassName('card-status')[itemToEdit];
                let cardPriority = document.getElementsByClassName('card-priority')[itemToEdit];
                let cardReason = document.getElementsByClassName('card-cause')[itemToEdit];

                modalDoctor.value = cardDoctor.innerText;
                modalDate.value = cardDate.innerText;
                modalName.value = cardName.innerText;
                modalAge.value = cardAge.innerText;
                modalStatus.value = cardStatus.innerText;
                modalPriority.value = cardPriority.innerText;
                modalReason.value = cardReason.innerText;

                modalWindow.classList.remove('dn');
                buttonWhichEditsCard.disabled = false;
                buttonWhichAddsCard.disabled = true;

                (async function invokeEditCard() {
                    const response = await fetch('http://localhost:4000/api/users/');

                    const users = await response.json();

                    buttonWhichEditsCard.onclick = function () {

                        let isValid = true;

                        [...inputValues].forEach(item => {
                            if (item.value.length < 1) {
                                isValid = false;
                            }
                        });

                        if (isValid) {
                            editUser(
                               users[itemToEdit]._id,
                               modalDoctor.value,
                               modalDate.value,
                               modalName.value,
                               modalAge.value,
                               modalStatus.value,
                               modalPriority.value,
                               modalReason.value
                            );

                            cardDoctor.innerText = modalDoctor.value;
                            cardDate.innerText = modalDate.value;
                            cardName.innerText = modalName.value;
                            cardAge.innerText = modalAge.value;
                            cardStatus.innerText = modalStatus.value;
                            cardPriority.innerText = modalPriority.value;
                            cardReason.innerText = modalReason.value;

                        }
                    }
                })()
            }

        });
    }

}

