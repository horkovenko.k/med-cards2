const mongoose = require("mongoose");
const express = require("express");
const cors = require("cors");
const Schema = mongoose.Schema;
const app = express();
const jsonParser = express.json();

const patientScheme = new Schema({
    doctor: String,
    date: String,
    name: String,
    age: String,
    status: String,
    priority: String,
    cause: String
}, {collection: 'appointment'} );
const Patient = mongoose.model("Patient", patientScheme);

app.use(cors());

let uri = 'mongodb+srv://u1:11@cluster0.vfsip.mongodb.net/Patients?retryWrites=true&w=majority\n';

mongoose.connect(uri, { useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false }, function(err){
    if(err) return console.log(err);
    app.listen(4000, function(){
        console.log("Сервер ожидает подключения...");
    });
});

// Adding new patient visit
app.post("/api/users", jsonParser, function (req, res) {

    if(!req.body) return res.sendStatus(400);

    const doctor = req.body.doctor;
    const date = req.body.date;
    const name = req.body.name;
    const age = req.body.age;
    const status = req.body.status;
    const priority = req.body.priority;
    const cause = req.body.cause;

    const user = new Patient({
        doctor: doctor,
        date: date,
        name: name,
        age: age,
        status: status,
        priority: priority,
        cause: cause
    });

    user.save(function(err){
        if(err) return console.log(err);
        res.send(user);
    });
});

// Get all users
app.get("/api/users", function(req, res){
    Patient.find({}, function(err, users){

        if(err) return console.log(err);
        res.send(users)
    });
});

app.delete("/api/users/:id", function(req, res){

    const id = req.params.id;

    Patient.findByIdAndDelete(id, function(err, user){

        if(err) return console.log(err);
        res.send(user);
    });

});

app.put("/api/users", jsonParser, function(req, res){

    if(!req.body) return res.sendStatus(400);

    const id = req.body.id;
    const doctor = req.body.doctor;
    const date = req.body.date;
    const name = req.body.name;
    const age = req.body.age;
    const status = req.body.status;
    const priority = req.body.priority;
    const cause = req.body.cause;

    const newPatient = {
        doctor: doctor,
        date: date,
        name: name,
        age: age,
        status: status,
        priority: priority,
        cause: cause
    };

    Patient.findOneAndUpdate({_id: id}, newPatient, {new: true}, function(err, user){
        if(err) return console.log(err);
        res.send(user);
    });
});